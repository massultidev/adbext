#!/usr/bin/env bash
# Bash completion script for adbext project.
# by P.J. Grochowski

# ---------------------------------------------------------------------
# This file is a part of adbext.
#
# The adbext is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The adbext is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with adbext.
# If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

function _adbext() {
    COMPREPLY=()
    local CURR="${COMP_WORDS[COMP_CWORD]}"
    local PREV="${COMP_WORDS[COMP_CWORD-1]}"
    local ADBEXT_BIN=adbext
    local ADBEXT_FUNC=_adbext_impl

    if [ "${PREV}" = "=" ] ; then
        PREV="${COMP_WORDS[COMP_CWORD-2]}"
    fi

    function _get_definition() {
        local SCRIPT_PATH="$(type -p $ADBEXT_BIN)"
        if [ -n "$SCRIPT_PATH" ] ; then
            cat "$SCRIPT_PATH"
        elif type "$ADBEXT_FUNC" >/dev/null 2>/dev/null ; then
            type "$ADBEXT_FUNC"
        fi
    }

    function _param_parser() {
        _get_definition | grep "${1}" | grep -v '_VALUE' | sed -e 's/=/ /g' -e 's/"//g' -e 's/;//g' | awk '{print $3}' | sed 's/|/ /g' | xargs
    }

    TARGETS="$(_get_definition | grep '_add_target' | grep -v '()' | awk '{print $2}' | sed 's/_/-/g' | xargs)"
    OPTIONS="$(_param_parser 'local OPT_')"

    local OPT_SERIAL OPT_HOST OPT_APK

    OPT_SERIAL="$(_param_parser 'local OPT_SERIAL')"
    OPT_HOST="$(_param_parser 'local OPT_HOST')"
    OPT_APK="$(_param_parser 'local OPT_APK')"

    local DIR_SSH SSH_CONFIG_FILE STRLIST_SSH_CONFIG_FILES

    DIR_SSH="${HOME}/.ssh"
    SSH_CONFIG_FILE="${DIR_SSH}/config"
    STRLIST_SSH_CONFIG_FILES="${SSH_CONFIG_FILE} ${DIR_SSH}/config.d/* $(grep '^Include' "${SSH_CONFIG_FILE}" 2>/dev/null | awk '{print $2}' | xargs)"

    local VAL_SERIAL VAL_HOST

    VAL_SERIAL="$(_get_definition | grep 'local OPT_SERIAL_VALUE_ALL' | sed -e 's/=/ /g' -e 's/"//g' -e 's/;//g' | awk '{print $3}' | sed 's/|/ /g' | xargs)"
    VAL_HOST=""
    for FILE in ${STRLIST_SSH_CONFIG_FILES} ; do
        VAL_HOST="${VAL_HOST} $(cd "${DIR_SSH}" && grep '^Host' "${FILE}" 2>/dev/null | grep -v '[?*]' | cut -d ' ' -f 2- | xargs)"
    done

    function _is_in_strlist() {
        local STRLIST STRKEY
        STRLIST="${1}"
        STRKEY="${2}"
        for ITEM in ${STRLIST} ; do
            if [ "${STRKEY}" = "${ITEM}" ] ; then
                return 0
            fi
        done
        return 1
    }

    if _is_in_strlist "${OPT_SERIAL}" "${PREV}" ; then
        # shellcheck disable=SC2207
        COMPREPLY=( $(compgen -W "${VAL_SERIAL}" "${CURR#*=}") )
        return 0
    fi
    if _is_in_strlist "${OPT_HOST}" "${PREV}" ; then
        # shellcheck disable=SC2207
        COMPREPLY=( $(compgen -W "${VAL_HOST}" "${CURR#*=}") )
        return 0
    fi
    if _is_in_strlist "${OPT_APK}" "${PREV}" ; then
        # shellcheck disable=SC2207
        COMPREPLY=( $(compgen -f "${CURR#*=}") )
        return 0
    fi
    if _is_in_strlist "${OPTIONS}" "${PREV}" ; then
        return 0
    fi
    if [ "${PREV}" = "adbext" ] ; then
        # shellcheck disable=SC2207
        COMPREPLY=( $(compgen -W "${TARGETS}" -- "${CURR}") )
        return 0
    fi
    # shellcheck disable=SC2207
    COMPREPLY=( $(compgen -W "${OPTIONS}" -- "${CURR}") )
}
complete -F _adbext adbext

