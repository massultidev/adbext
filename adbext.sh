#!/usr/bin/env bash
# adbext - Bash extensions for adb.
# by P.J. Grochowski

# ---------------------------------------------------------------------
# This file is a part of adbext.
#
# The adbext is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The adbext is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with adbext.
# If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

function _adbext_impl() {(
    local APP_NAME="${ADBEXT}"
    local APP_VERSION="v.1.2.2"
    local MSG_HELP=""
    local LIST_TARGET=()

    local TOP_PID=${BASHPID}
    trap "echo ; exit 1" TERM INT
    function _terminate() {
        if [ "${DEBUG}" = "1" ] ; then
            echo "\${FUNCNAME[*]}: ${FUNCNAME[*]}"
        fi
        if [ "${1}" != "" ] ; then
            _echo_error "${1}" >&2
        fi
        kill -s TERM ${TOP_PID}
        exit 1
    }

    local NULL="/dev/null"

    local COLOR_NONE=""
    local COLOR_RED=""
    local COLOR_YELLOW=""

    local EXE_BASH="bash"
    local EXE_PRINTF="printf"
    local EXE_SED="sed"
    local EXE_SSH="ssh"
    local EXE_SCP="scp"
    local EXE_SSHPASS="sshpass"
    local EXE_ADB="adb"
    local EXE_GREP="grep"
    local EXE_AWK="awk"
    local EXE_TAIL="tail"
    local EXE_XARGS="xargs"
    local EXE_SORT="sort"
    local EXE_MKTEMP="mktemp"
    local EXE_BASENAME="basename"
    local EXE_WC="wc"
    local EXE_TR="tr"

    local OPT_HOST="-H|--host"         ; local OPT_HOST_VALUE=""
    local OPT_USER="-U|--user"         ; local OPT_USER_VALUE=""
    local OPT_PASS="-P|--pass"         ; local OPT_PASS_VALUE=""
    local OPT_SERIAL="-s|--serial"     ; local OPT_SERIAL_VALUE=""
    local OPT_EXCLUDE="-e|--exclude"   ; local OPT_EXCLUDE_VALUE=""
    local OPT_PACKAGE="-p|--package"   ; local OPT_PACKAGE_VALUE=""
    local OPT_ADB_ARGS="-a|--adb-args" ; local OPT_ADB_ARGS_VALUE=""
    local OPT_APK="--apk"              ; local OPT_APK_VALUE=""

    local OPT_SERIAL_VALUE_ALL="all"

    local LBL_CRITICAL="[CRITICAL]:"
    local LBL_ERROR="[ERROR]:"
    local LBL_WARNING="[WARNING]:"

    # Publicly declare environment variables:
    DEBUG="${DEBUG}"
    ADBEXT_SSHPASS="${ADBEXT_SSHPASS}"
    ADBEXT_PACKAGE="${ADBEXT_PACKAGE}"

    # Set default values from environment variables:
    OPT_PASS_VALUE="${ADBEXT_SSHPASS}"
    OPT_PACKAGE_VALUE="${ADBEXT_PACKAGE}"

    local LIST_EXE_CRITICAL=(
        "${EXE_BASH}"
        "${EXE_PRINTF}"
        "${EXE_SED}"
        "${EXE_SSH}"
        "${EXE_SCP}"
        "${EXE_ADB}"
        "${EXE_GREP}"
        "${EXE_AWK}"
        "${EXE_TAIL}"
        "${EXE_XARGS}"
        "${EXE_SORT}"
        "${EXE_MKTEMP}"
        "${EXE_BASENAME}"
        "${EXE_WC}"
        "${EXE_TR}"
    )
    local LIST_EXE_EXTRA=(
        "${EXE_SSHPASS}:${OPT_PASS}/ADBEXT_SSHPASS"
    )

    function _is_debug() {
        if [ "${DEBUG}" != "1" ] ; then
            return 1
        fi
    }

    if _is_debug ; then
        echo "\$*: $*"
    fi

    function __verify_exe() {
        command -v "${1}" 2> ${NULL} > ${NULL}
        return $?
    }
    function __verify_colors_available() {
        local NCOLORS=""
        if __verify_exe test ; then
            if __verify_exe tput ; then
                if test -t 1 ; then
                    NCOLORS="$(tput colors)"
                    if test -n "${NCOLORS}" && test "${NCOLORS}" -ge 8; then
                        COLOR_NONE="$(tput sgr0)"
                        COLOR_RED="$(tput setaf 1)"
                        COLOR_YELLOW="$(tput setaf 3)"
                    fi
                fi
            fi
        fi
    }
    __verify_colors_available

    function _echo_error() {
        echo -e "${COLOR_RED}${1}${COLOR_NONE}" >&2
    }
    function _echo_warning() {
        echo -e "${COLOR_YELLOW}${1}${COLOR_NONE}" >&2
    }

    function _verify_exe_critical() {
        local FLAG_ERROR=0
        for EXE in "${LIST_EXE_CRITICAL[@]}" ; do
            if ! __verify_exe "${EXE}" ; then
                ((FLAG_ERROR=FLAG_ERROR+1))
                _echo_error "${LBL_ERROR} Could not find application: \"${EXE}\""
            fi
        done
        if [ "${FLAG_ERROR}" != "0" ] ; then
            _echo_error "${LBL_CRITICAL} Application will be terminated. Error count: ${FLAG_ERROR}"
            exit 1
        fi
    }
    function _verify_exe_extra() {
        for LIST_EXE_EXTRA_ENTRY in "${LIST_EXE_EXTRA[@]}" ; do
            local EXE="${LIST_EXE_EXTRA_ENTRY%:*}"
            local PARAMS="${LIST_EXE_EXTRA_ENTRY#*:}"
            local OPT="${PARAMS%/*}"
            local ENV="${PARAMS#*/}"
            if ! __verify_exe "${EXE}" ; then
                _echo_warning "${LBL_WARNING} Could not find application \"${EXE}\". Option \"${OPT}\" and variable \"${ENV}\" will not work!"
                            fi
        done
    }
    _verify_exe_critical
    _verify_exe_extra

    function _usage_add_header() {
        MSG_HELP+="$( \
            )Usage (${APP_VERSION}): \\n$( \
            )    ${APP_NAME} <target> <parameters>\\n$( \
        )"
    }
    function _usage_add_section_targets() {
        MSG_HELP+="$( \
            )\\n$( \
            )Available targets:\\n$( \
        )"
    }
    function _usage_add_section_options() {
        MSG_HELP+="$( \
            )\\n$( \
            )Available options (Warn: They always override environment variables!):\\n$( \
        )"
    }
    function _usage_add_section_variables() {
        MSG_HELP+="$( \
            )\\n$( \
            )Enviroment variables that change applications behaviour:\\n$( \
        )"
    }
    function _usage_add_section_entry() {
        MSG_HELP+=$(printf '    %-20s' "${1}")
        MSG_HELP+=" - ${2}\\n"
    }
    function _usage_print() {
        echo -e "${MSG_HELP}"
        exit 1
    }

    function _add_target() {
        LIST_TARGET+=("${1}")
        _usage_add_section_entry "${1//_/-}" "${2}"
    }
    function _add_option() {
        _usage_add_section_entry "${1}" "${2}"
    }
    function _add_environment_variable() {
        _usage_add_section_entry "${1}" "${2}"
    }

    function _verify_target() {
        if [ "${1}" = "" ] ; then
            _usage_print
        fi
        echo "${LIST_TARGET[@]}" | grep "\\b${1}\\b" > ${NULL} || _usage_print
    }
    function _gen_case() {
        echo "${1}) ${2}=\"\${VAL}\" ; shift ;; $(echo "${1}" | sed -e 's/|/=\*|/g' -e 's/$/=\*/g')) ${2}=\"\${KEY#*=}\" ;;"
    }

    _usage_add_header

    _usage_add_section_targets

    _add_target custom             "Run classic adb command on series of devices. Requires option \"${OPT_ADB_ARGS}\" for proper functioning."
    _add_target devices            "Print list of available devices."
    _add_target reboot             "Reboot selected device/s."
    _add_target install            "Install specified APK file on selected device/s."
    _add_target uninstall          "Remove application with specified package name on selected device/s."

    _add_target app_log            "Get logcat generated only by specified package. Works only for single device at a time."
    _add_target app_kill           "Kill application with specified package name on selected device/s."
    _add_target app_start          "Start application with specified package name on selected device/s."
    _add_target app_restart        "Restart application with specified package name on selected device/s."

    _add_target get_app_version    "Get version of application with specified package name on selected device/s."
    _add_target get_app_info       "Get information about application with specified package name on selected device/s."
    _add_target get_sys_version    "Print Android version and API level for selected device/s."
    _add_target get_sys_info       "Print device information for selected device/s."
    _add_target get_default_sms    "Get packege name of application currently set as default text message handler."
    _add_target get_default_dialer "Get packege name of application currently set as default dialer."

    _add_target set_default_sms    "Set application with specified package name as default text message handler."
    _add_target set_default_dialer "Set application with specified package name as default dialer."

    _usage_add_section_options

    _add_option "${OPT_HOST}"     "Takes remote host IP or user@IP or SSH alias."
    _add_option "${OPT_USER}"     "Takes remote host user. Should be omitted if \"${OPT_HOST}\" provided in user@IP format."
    _add_option "${OPT_PASS}"     "Takes remote host password. (Warn: Unsafe! Use \"ADBEXT_SSHPASS\" or private key authentication instead!)"
    _add_option "${OPT_SERIAL}"   "Takes device serial number/s or \"${OPT_SERIAL_VALUE_ALL}\"."
    _add_option "${OPT_EXCLUDE}"  "Matches device serial number/s as excluded from target."
    _add_option "${OPT_PACKAGE}"  "Performs target for provided package name."
    _add_option "${OPT_ADB_ARGS}" "Takes arguments to pass to original adb command."
    _add_option "${OPT_APK}"      "Takes path to APK file. Used only by \"install\" target."

    _usage_add_section_variables

    _add_environment_variable ADBEXT_SSHPASS "Provides more secure way of passing ssh password than parameter \"${OPT_PASS}\"."
    _add_environment_variable ADBEXT_PACKAGE "Alternative for paramter \"${OPT_PACKAGE}\"."

    # Save target and shift arguments:
    local TARGET="${1//-/_}"
    shift
    while [[ $# -gt 0 ]] ; do
        local KEY="${1}"
        # shellcheck disable=SC2034
        local VAL="${2}"

        shift
        eval "case \"${KEY}\" in
            $(_gen_case ${OPT_HOST}     OPT_HOST_VALUE    )
            $(_gen_case ${OPT_USER}     OPT_USER_VALUE    )
            $(_gen_case ${OPT_PASS}     OPT_PASS_VALUE    )
            $(_gen_case ${OPT_SERIAL}   OPT_SERIAL_VALUE  )
            $(_gen_case ${OPT_EXCLUDE}  OPT_EXCLUDE_VALUE )
            $(_gen_case ${OPT_PACKAGE}  OPT_PACKAGE_VALUE )
            $(_gen_case ${OPT_ADB_ARGS} OPT_ADB_ARGS_VALUE)
            $(_gen_case ${OPT_APK}      OPT_APK_VALUE     )
        esac"
    done

    if _is_debug ; then
        echo "\${TARGET} : ${TARGET}"
        echo

        local PASSWORD_HEXED
        PASSWORD_HEXED="$(echo "${OPT_PASS_VALUE}" | tr '[:print:]' '*')"

        printf "%-21s : ${OPT_HOST_VALUE}     \\n" "\${OPT_HOST_VALUE}"
        printf "%-21s : ${OPT_USER_VALUE}     \\n" "\${OPT_USER_VALUE}"
        printf "%-21s : ${PASSWORD_HEXED}     \\n" "\${OPT_PASS_VALUE}"
        printf "%-21s : ${OPT_SERIAL_VALUE}   \\n" "\${OPT_SERIAL_VALUE}"
        printf "%-21s : ${OPT_EXCLUDE_VALUE}  \\n" "\${OPT_EXCLUDE_VALUE}"
        printf "%-21s : ${OPT_PACKAGE_VALUE}  \\n" "\${OPT_PACKAGE_VALUE}"
        printf "%-21s : ${OPT_ADB_ARGS_VALUE} \\n" "\${OPT_ADB_ARGS_VALUE}"
        echo
    fi

    _verify_target "${TARGET}"

    local PREFIX_REMOTE=""
    local SSH=""
    local SCP=""

    function _process_options() {
        if [ "${OPT_HOST_VALUE}" = "" ] ; then
            return 0
        fi
        if [ "${OPT_USER_VALUE}" = "" ] ; then
            PREFIX_REMOTE="${OPT_HOST_VALUE}"
        else
            PREFIX_REMOTE="${OPT_USER_VALUE}@${OPT_HOST_VALUE}"
        fi
        SSH="${EXE_SSH}"
        SCP="${EXE_SCP}"
        if [ "${OPT_PASS_VALUE}" != "" ] ; then
            export SSHPASS=${OPT_PASS_VALUE}
            SSH="${EXE_SSHPASS} -e ${SSH}"
            SCP="${EXE_SSHPASS} -e ${SCP}"
        fi
    }
    _process_options

    if _is_debug ; then
        printf "%-16s : ${PREFIX_REMOTE}\\n" "\${PREFIX_REMOTE}"
        printf "%-16s : ${SSH}          \\n" "\${SSH}"
        printf "%-16s : ${SCP}          \\n" "\${SCP}"
        echo
    fi

    function _get_liststr_length() {
        echo "${1}" | tr ' ' '\n' | sed '/^\s*$/d' | wc -l
    }
    function _is_remote() {
        if [ -z "${PREFIX_REMOTE}" ] ; then
            return 1
        fi
    }
    function _run_command() {
        local CMD
        CMD="${1}"
        if ! _is_remote ; then
            bash -c "${CMD}" || exit 1
            return 0
        fi
        ${SSH} ${PREFIX_REMOTE} "${CMD}"  | tr -d '\r' || exit 1
    }
    function _copy_file() {
        local NAME_FILE
        NAME_FILE="$(basename "${1}")"
        if _is_remote ; then
            echo " -- Coping file \"${NAME_FILE}\" to: ${PREFIX_REMOTE}:${2}"
            ${SCP} "${1}" ${PREFIX_REMOTE}:"${2}/"
            return 0
        fi
        echo " -- Coping file \"${NAME_FILE}\" to: ${2}"
        cp -fv "${1}" "${2}/"
    }
    function _get_dir_tmp() {
        local DIR_TMP
        DIR_TMP="$(dirname "$(mktemp tmp.XXXXXXXXXX -ut)")"
        if [ "${DIR_TMP}" = "" ] ; then
            _terminate "${LBL_ERROR} Could not acquire tmp directory path!"
        fi
        echo "${DIR_TMP}"
    }
    function _get_all_devices_liststr() {
        local CMD="adb start-server >${NULL} 2>${NULL} ; adb devices"
        _run_command "${CMD}" | tail -n +2 | grep -v '^$' | awk '{print $1}' | xargs || exit 1
    }
    function _get_chosen_devices_liststr() {
        local OPT_SERIAL_VALUE_ORG
        OPT_SERIAL_VALUE_ORG="${OPT_SERIAL_VALUE}"

        if [ "${OPT_SERIAL_VALUE}" = "" ] || [ "${OPT_SERIAL_VALUE}" = "${OPT_SERIAL_VALUE_ALL}" ] ; then
            OPT_SERIAL_VALUE="$(_get_all_devices_liststr)"
        fi

        local SERIAL_COUNT
        SERIAL_COUNT="$(_get_liststr_length "${OPT_SERIAL_VALUE}")"

        if [ "${SERIAL_COUNT}" = "0" ] ; then
            _terminate "${LBL_ERROR} There are no devices in specified location!"
        fi

        if [ "${OPT_SERIAL_VALUE_ORG}" = "" ] && [ "${SERIAL_COUNT}" != "1" ] ; then
            _terminate "${LBL_ERROR} There are multiple devices. You must specify serial number/s or \"${OPT_SERIAL_VALUE_ALL}\" for this target!"
        fi

        if [ "${OPT_EXCLUDE_VALUE}" != "" ] ; then
            for EXCLUDE in ${OPT_EXCLUDE_VALUE} ; do
                OPT_SERIAL_VALUE="${OPT_SERIAL_VALUE//${EXCLUDE}/}"
            done
        fi

        echo "${OPT_SERIAL_VALUE}"
    }
    function _notify_devices() {
        echo " -- Running target for devices:"
        echo "${1}" | tr ' ' '\n' | sed '/^\s*$/d'
        echo
    }
    function _notify_package() {
        echo " -- Running target for package: ${OPT_PACKAGE_VALUE}"
        echo
    }
    function _verify_package() {
        if [ "${OPT_PACKAGE_VALUE}" = "" ] ; then
            _terminate "${LBL_ERROR} You must specify package name for this target!"
        fi
        _notify_package
    }
    function _verify_devices_single() {
        local SERIAL_COUNT
        SERIAL_COUNT="$(_get_liststr_length "${1}")"
        if [ "${SERIAL_COUNT}" != "1" ] ; then
            _terminate "${LBL_ERROR} This target only supports single device operations!"
        fi
    }
    function _verify_apk() {
        if [ "${OPT_APK_VALUE}" = "" ] ; then
            _terminate "${LBL_ERROR} You must specify APK file path for this target!"
        fi
        if [ ! -f "${OPT_APK_VALUE}" ] ; then
            _terminate "${LBL_ERROR} Provided APK file does not exist!"
        fi
    }
    function _reboot() {
        local DEVICE
        DEVICE="${1}"

        local CMD
        CMD="adb -s ${DEVICE} reboot"

        echo " -- Rebooting."
        _run_command "${CMD}"
    }
    function _app_start() {
        local CMD="adb -s ${1} shell monkey -p ${OPT_PACKAGE_VALUE} -c android.intent.category.LAUNCHER 1"
        echo " -- Starting application."
        _run_command "${CMD}"
    }
    function _app_kill() {
        local CMD="adb -s ${1} shell am force-stop ${OPT_PACKAGE_VALUE}"
        echo " -- Killing application."
        _run_command "${CMD}"
    }
    function _set_app_default() {
        local CMD="adb -s ${1} shell settings put secure ${2} ${OPT_PACKAGE_VALUE}"
        _run_command "${CMD}"
    }
    function _set_app_default_dialer() {
        echo " -- Setting default application: dialer"
        _set_app_default "${1}" dialer_default_application
    }
    function _set_app_default_sms() {
        echo " -- Setting default application: sms"
        _set_app_default "${1}" sms_default_application
    }
    function _get_app_default() {
        local CMD="adb -s ${1} shell settings get secure ${2}"
        _run_command "${CMD}"
    }
    function _get_app_default_dialer() {
        echo " -- Getting default application: dialer"
        _get_app_default "${1}" dialer_default_application
    }
    function _get_app_default_sms() {
        echo " -- Getting default application: sms"
        _get_app_default "${1}" sms_default_application
    }

    function _dump_package() {
        local DEVICE LISTSTR_ENTRIES
        DEVICE="${1}"
        LISTSTR_ENTRIES="${2}"

        local X_DIM CMD
        X_DIM=30

        CMD="adb -s ${DEVICE} shell dumpsys package ${OPT_PACKAGE_VALUE}"
        # shellcheck disable=SC2046
        _run_command "${CMD}"  | tr ' ' '\n' | grep -v '^$' | grep --color=never \
            $(echo "${LISTSTR_ENTRIES}" | tr ' ' '\n' | sed -e 's/^/\-e /g' | tr '\n' ' ') \
        | sort \
        | while read -r LINE ; do
            if [ "${LINE}" != "" ] ; then
                local MSG PROP_PARAM PROP_VALUE
                PROP_PARAM="${LINE%%=*}"
                PROP_VALUE="${LINE#*=}"
                MSG=$(printf "%-${X_DIM}s" "${PROP_PARAM}")
                MSG+=" - ${PROP_VALUE}"
                echo "${MSG}"
            fi
        done
    }
    local _DUMP_PACKAGE_VERSION_NAME _DUMP_PACKAGE_VERSION_CODE _DUMP_PACKAGE_TARGET_SDK _DUMP_PACKAGE_CODE_PATH
    _DUMP_PACKAGE_VERSION_NAME="versionName"
    _DUMP_PACKAGE_VERSION_CODE="versionCode"
    _DUMP_PACKAGE_TARGET_SDK="targetSdk"
    _DUMP_PACKAGE_CODE_PATH="codePath"

    function _dump_getprop() {
        local DEVICE LISTSTR_ENTRIES
        DEVICE="${1}"
        LISTSTR_ENTRIES="${2}"

        local X_DIM CMD
        X_DIM=30

        CMD="adb -s ${DEVICE} shell getprop"
        # shellcheck disable=SC2046
        _run_command "${CMD}"  | grep \
            $(echo "${LISTSTR_ENTRIES}" | tr ' ' '\n' | sed -e 's/$/\.\:/g' -e 's/^/\-e /g' | tr '\n' ' ') \
        | sort | tr -d ']:[' \
        | while read -r LINE ; do
            if [ "${LINE}" != "" ] ; then
                local MSG PROP_PARAM PROP_VALUE
                PROP_PARAM="$(echo "${LINE}" | awk '{print $1}')"
                PROP_VALUE="$(echo "${LINE}" | awk '{$1=""; print $0}')"
                MSG=$(printf "%-${X_DIM}s" "${PROP_PARAM}")
                MSG+=" - ${PROP_VALUE}"
                echo "${MSG}"
            fi
        done
    }
    local _DUMP_GETPROP_VERSION_NAME _DUMP_GETPROP_VERSION_CODE _DUMP_GETPROP_BRAND _DUMP_GETPROP_MANUFACTURER _DUMP_GETPROP_MODEL
    _DUMP_GETPROP_VERSION_NAME="ro.build.version.release"
    _DUMP_GETPROP_VERSION_CODE="ro.build.version.sdk"
    _DUMP_GETPROP_BRAND="ro.product.brand"
    _DUMP_GETPROP_MANUFACTURER="ro.product.manufacturer"
    _DUMP_GETPROP_MODEL="ro.product.model"

    function _run_for_multiple_devices() {
        local CALLBACK_INTRO CALLBACK_LOOP
        CALLBACK_INTRO="${1}"
        CALLBACK_LOOP="${2}"

        local LISTSTR_DEVICES
        LISTSTR_DEVICES="$(_get_chosen_devices_liststr)"

        _notify_devices "${LISTSTR_DEVICES}"

        ${CALLBACK_INTRO}

        for DEVICE in ${LISTSTR_DEVICES} ; do
            echo " -- Device: ${DEVICE}"
            ${CALLBACK_LOOP} "${DEVICE}"
            echo
        done
    }

    # Targets: General
    function custom() {
        function _callback_intro() {
            echo " -- Running custom command:"
            echo "${OPT_ADB_ARGS_VALUE}"
            echo
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"

            local CMD
            CMD="adb -s ${DEVICE} ${OPT_ADB_ARGS_VALUE}"

            _run_command "${CMD}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function devices() {
        local LISTSTR_DEVICES
        LISTSTR_DEVICES="$(_get_all_devices_liststr)"

        local X_DIM
        X_DIM="$(echo "${LISTSTR_DEVICES}" | tr ' ' '\n' | wc -L)"
        ((X_DIM=X_DIM + 5))

        local CMD="adb start-server >${NULL} 2>${NULL} ; adb devices"
        _run_command "${CMD}" | tail -n +2 | grep -v '^$' | sed '/^\s*$/d' \
        | while read -r LINE ; do
            if [ "${LINE}" != "" ] ; then
                local MSG DEVICE STATUS
                DEVICE="$(echo "${LINE}" | awk '{print $1}')"
                STATUS="$(echo "${LINE}" | awk '{$1=""; print $0}')"
                MSG=$(printf "%-${X_DIM}s" "${DEVICE}")
                MSG+=" - ${STATUS}"
                echo "${MSG}"
            fi
        done
    }
    function reboot() {
        function _callback_intro() {
            :
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _reboot "${DEVICE}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function install() {
        local NAME_FILE_APK DIR_TMP
        NAME_FILE_APK="$(basename "${OPT_APK_VALUE}")"
        DIR_TMP="$(_get_dir_tmp)"

        function _callback_intro() {
            _verify_apk
            _copy_file "${OPT_APK_VALUE}" "${DIR_TMP}"
            echo
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"

            local CMD
            CMD="cd \"${DIR_TMP}\" && adb -s ${DEVICE} install ${OPT_ADB_ARGS_VALUE} \"./${NAME_FILE_APK}\""

            echo " -- Installing APK."
            _run_command "${CMD}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function uninstall() {
        function _callback_intro() {
            _verify_package
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"

            local CMD
            CMD="adb -s ${DEVICE} uninstall ${OPT_ADB_ARGS_VALUE} ${OPT_PACKAGE_VALUE}"

            echo " -- Uninstalling application."
            _run_command "${CMD}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }

    # Targets: Application
    function app_log() {
        local LISTSTR_DEVICES
        LISTSTR_DEVICES="$(_get_chosen_devices_liststr)"

        _verify_devices_single "${LISTSTR_DEVICES}"

        _notify_devices "${LISTSTR_DEVICES}"

        _verify_package

        local DEVICE CMD_0 CMD_1 CMD_2 PID_LOGCAT PID_PACKAGE_OLD
        DEVICE="${LISTSTR_DEVICES}"

        echo " -- Starting capturing log from device: ${DEVICE}"

        CMD_0="adb -s ${DEVICE} logcat -v threadtime"
        CMD_1="adb -s ${DEVICE} shell pidof -s ${OPT_PACKAGE_VALUE}"
        CMD_2="adb -s ${DEVICE} shell ps | grep ${OPT_PACKAGE_VALUE}"

        PID_LOGCAT=
        PID_PACKAGE_OLD=
        while : ; do
            local PID_PACKAGE
            PID_PACKAGE="$(_run_command "${CMD_1}")"
            if [ "${PID_PACKAGE}" = "" ] || echo "${PID_PACKAGE}" | grep -i "not found" > /dev/null ; then
                PID_PACKAGE="$(_run_command "${CMD_2}")"
                PID_PACKAGE="$(echo "${PID_PACKAGE}" | awk '{print $2}')"
            fi
            case ${PID_PACKAGE} in
                [0-9]*)
                    if [ "${PID_PACKAGE}" != "${PID_PACKAGE_OLD}" ] ; then
                        if [ "${PID_LOGCAT}" != "" ] ; then
                            echo
                            echo " -- Process ID has changed!"
                            echo
                            echo " -- Killing previous logcat process:"
                            _run_command "kill ${PID_LOGCAT}"
                            echo
                            echo " -- Cleaning logcat message buffers:"
                            _run_command "adb -s ${DEVICE} logcat -c"
                        fi
                        echo
                        echo " -- Starting logcat live feed:"
                        (_run_command "${CMD_0} --pid=\"${PID_PACKAGE}\"" ; echo "Broken pipe!")& PID_LOGCAT=$!
                        PID_PACKAGE_OLD=${PID_PACKAGE}
                    fi
                ;;
            esac
        done
        echo " -- Could no longer read from logcat!"
    }
    function app_kill() {
        function _callback_intro() {
            _verify_package
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _app_kill "${DEVICE}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function app_start() {
        function _callback_intro() {
            _verify_package
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _app_start "${DEVICE}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function app_restart() {
        function _callback_intro() {
            _verify_package
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _app_kill "${DEVICE}"
            _app_start "${DEVICE}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }

    # Targets: Getters
    function get_app_version() {
        function _callback_intro() {
            _verify_package
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _dump_package "${DEVICE}" "${_DUMP_PACKAGE_VERSION_CODE} ${_DUMP_PACKAGE_VERSION_NAME}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function get_app_info() {
        function _callback_intro() {
            _verify_package
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _dump_package "${DEVICE}" "${_DUMP_PACKAGE_VERSION_CODE} ${_DUMP_PACKAGE_VERSION_NAME} ${_DUMP_PACKAGE_TARGET_SDK} ${_DUMP_PACKAGE_CODE_PATH}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function get_sys_version() {
        function _callback_intro() {
            :
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _dump_getprop "${DEVICE}" "${_DUMP_GETPROP_VERSION_NAME} ${_DUMP_GETPROP_VERSION_CODE}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function get_sys_info() {
        function _callback_intro() {
            :
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _dump_getprop "${DEVICE}" "${_DUMP_GETPROP_VERSION_NAME} ${_DUMP_GETPROP_VERSION_CODE} ${_DUMP_GETPROP_BRAND} ${_DUMP_GETPROP_MANUFACTURER} ${_DUMP_GETPROP_MODEL}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function get_default_sms() {
        function _callback_intro() {
            :
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _get_app_default_sms "${DEVICE}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function get_default_dialer() {
        function _callback_intro() {
            :
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _get_app_default_dialer "${DEVICE}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }

    # Targets: Setters
    function set_default_sms() {
        function _callback_intro() {
            _verify_package
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _set_app_default_sms "${DEVICE}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }
    function set_default_dialer() {
        function _callback_intro() {
            _verify_package
        }
        function _callback_loop() {
            local DEVICE
            DEVICE="${1}"
            _set_app_default_dialer "${DEVICE}"
        }
        _run_for_multiple_devices _callback_intro _callback_loop
    }

    ${TARGET}
)}

function adbext() {(
    local ADBEXT="${FUNCNAME[0]}"
    _adbext_impl "$@"
)}

# Run only if not being sourced:
if [[ "${BASH_SOURCE[0]}" = "${0}" ]] ; then
    adbext "$@"
fi

