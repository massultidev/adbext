# Project: "adbext"
- - -
Project adbext is a bash utility aimed for Android developers.

## Features:
* Support for single action on multiple devices.
* Actions on devices connected both to local and remote machines.
* Commonly used debug bridge command sequences packed in purpose targeted and intuitive methods.

## Requirements:
* Bash command line interpreter.
* Android Debug Bridge (a.k.a "adb"). Installed and accessible with PATH environment variable.

- - -
### Installation:
* Clone latest release tag to the place of your liking. Let's say: ~/.apps/adbext
* Add following lines to your .bashrc (their order is important):
>
>source ~/.apps/adbext/adbext.sh
>
>source ~/.apps/adbext/completion.sh

### Tips:
* The script verifies its dependencies presence in every run. So if you face the the error at the start don't panic. Just read its content and install missing packages.
* After sourcing completion.sh script you should be able to complete adbext paramters with TAB key.
* You can omit sourcing completion.sh if you do not need TAB completion.
* To access usage notes just run adbext without any parameters.
* If you frequently work with the same Android package just fill variable ADBEXT_PACKAGE with its name in your .bashrc or some other environment script.
* It is discoureged to pass SSH password to adbext command. Why not use ssh-copy-id in order to always authenticate safely with private key?

### Development and CI:
* This project is being developed with shellcheck static code analysis.
* Application has been tested on:
    * Arch Linux
    * Manjaro Linux

